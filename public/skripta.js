window.addEventListener('load', function() {
	//stran nalozena

	var prizgiCakanje = function() {
		document.querySelector(".loading").style.display = "block";
	};

	var ugasniCakanje = function() {
		document.querySelector(".loading").style.display = "none";
	};

	document.querySelector("#nalozi").addEventListener("click", prizgiCakanje);

	//Pridobi seznam datotek
	var pridobiSeznamDatotek = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		xhttp.onload = function (event) {console.log("zahteva za branje")};
		xhttp.open('get', '/datoteke', true);
		xhttp.send();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				var datoteke = JSON.parse(xhttp.responseText);

				var datotekeHTML = document.querySelector("#datoteke");

				for (var i=0; i<datoteke.length; i++) {
					var datoteka = datoteke[i];

					var velikost = datoteka.velikost;
					var enota = "B";
					var enote = ["KiB", "MiB", "GiB"];
					
					var s = 0;
					while(velikost>1024) {
						velikost = velikost / 1024;
						enota = enote[s];
						s++;
						console.log(velikost+"____"+enota);
					}

					datotekeHTML.innerHTML += " \
						<div class='datoteka senca rob'> \
							<div class='naziv_datoteke'> " + datoteka.datoteka + "  (" + velikost + " " + enota + ") </div> \
							<div class='akcije'> \
							<span><a href='/poglej/" + datoteka.datoteka + "' target='_self'>Poglej</a></span> \
							| <span><a href='/prenesi/" + datoteka.datoteka + "' target='_self'>Prenesi</a></span> \
							| <span akcija='brisi' datoteka='"+ datoteka.datoteka + "'>Izbriši</span> </div> \
					    </div>";
				}

				if (datoteke.length > 0) {
					var brisanje = document.querySelectorAll("span[akcija=brisi]");
    				for (var i = 0; i < brisanje.length; i++)
    					brisanje[i].addEventListener("click",brisi);
				}
				ugasniCakanje();
			}
		};
	};
	pridobiSeznamDatotek();

	var brisi = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				if (xhttp.responseText == "Datoteka izbrisana") {
					window.location = "/";
				} else {
					alert("Datoteke ni bilo možno izbrisati!");
				}
			}
			ugasniCakanje();
		};
		xhttp.open("GET", "/brisi/" + this.getAttribute("datoteka"), true);
		xhttp.send();
	};

});
